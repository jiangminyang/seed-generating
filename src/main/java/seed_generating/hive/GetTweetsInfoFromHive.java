package seed_generating.hive;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.hive.HiveContext;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import scala.Tuple2;
import seed_generating.utils.Pair;

public class GetTweetsInfoFromHive {
	static String APP_NAME = "GeoLocation.seedGenerating";
	static SparkConf sparkConf;
	static JavaSparkContext sparkContext;
	static HiveContext hiveContext;
	static final int NUM_TASKS = 150;

	static {
		sparkConf = new SparkConf().setAppName(APP_NAME);
		sparkContext = new JavaSparkContext(sparkConf);
		sparkContext.hadoopConfiguration().set("mapreduce.input.fileinputformat.input.dir.recursive", "true");
		hiveContext = new HiveContext(sparkContext.sc());
		System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
	}
	
	public static JavaPairRDD<Long, Pair<List<Pair<String, Double>>, Long>> getUserToLocData(int pastDays, double gamma) {
		System.out.println("Start getting data from Hive at " + (new Date()).toString());
		hiveContext.sql("USE lookup_tables");
		DataFrame data = hiveContext.sql("SELECT id, place_name, country_code FROM tweets_geo WHERE datediff(from_unixtime(unix_timestamp(), 'yyyy-MM-dd'), to_date(date)) < " + pastDays);
		System.out.println("Finish getting data from Hive at " + (new Date()).toString());
		System.out.println("Start processing data at " + (new Date()).toString());
		JavaPairRDD<Long, String> pairData = data.javaRDD().mapToPair(new PairFunction<Row, Long, String>() {

			public Tuple2<Long, String> call(Row row) throws Exception {
				return new Tuple2<Long, String>(row.getLong(0), row.getString(1) + ", " + row.getString(2));
			}
			
		});
		JavaPairRDD<Long, Pair<List<Pair<String, Double>>, Long>> computedResult = mapReduce(pairData);
		final Broadcast<Double> broadcastVarGamma = sparkContext.broadcast(gamma);
		JavaPairRDD<Long, Pair<List<Pair<String, Double>>, Long>> result = computedResult.filter(new Function<Tuple2<Long, Pair<List<Pair<String, Double>>, Long>>, Boolean>() {

			public Boolean call(Tuple2<Long, Pair<List<Pair<String, Double>>, Long>> row) throws Exception {
				if (row._2.getB() < 5) return false;
				double max = 0.0;
				for(int i = 0; i < row._2.getA().size(); i++) {
					if (row._2.getA().get(i).getB() > max) {
						max = row._2.getA().get(i).getB();
					}
				}
				return (max >= broadcastVarGamma.value());
			}
			
		}).mapToPair(new PairFunction<Tuple2<Long, Pair<List<Pair<String, Double>>, Long>>, Long, Pair<List<Pair<String, Double>>, Long>>() {

			public Tuple2<Long, Pair<List<Pair<String, Double>>, Long>> call(Tuple2<Long, Pair<List<Pair<String, Double>>, Long>> row)
					throws Exception {
				Collections.sort(row._2.getA(), new Comparator<Pair<String, Double>>() {

					public int compare(Pair<String, Double> a, Pair<String, Double> b) {
						if (b.getB() < a.getB()) return -1;
						if (b.getB() > a.getB()) return 1;
						return 0;
					}
					
				});
				return row;
			}
			
		});
		System.out.println("Finish processing data at " + (new Date()).toString());
		return result;
	}
	
	private static JavaPairRDD<Long, Pair<List<Pair<String, Double>>, Long>> mapReduce(JavaPairRDD<Long, String> data) {
		JavaPairRDD<Long, Pair<List<Pair<String, Double>>, Long>> result = data.groupByKey().mapToPair(new PairFunction<Tuple2<Long, Iterable<String>>, Long, Pair<List<Pair<String, Double>>, Long>>() {

			public Tuple2<Long, Pair<List<Pair<String, Double>>, Long>> call(Tuple2<Long, Iterable<String>> row) throws Exception {
				Map<String, Integer> LocCount = new HashMap<String, Integer>();
				long count = 0;
				for(String place_name : row._2) {
					count++;
					if (LocCount.containsKey(place_name)) {
						LocCount.put(place_name, LocCount.get(place_name) + 1);
					}
					else {
						LocCount.put(place_name, 1);
					}
				}
				List<Pair<String, Double>> result = new ArrayList<Pair<String, Double>>();
				for(String place_name : LocCount.keySet()) {
					result.add(new Pair<String, Double>(place_name, LocCount.get(place_name) / (count * 1.0)));
				}
				return new Tuple2<Long, Pair<List<Pair<String, Double>>, Long>>(row._1, new Pair<List<Pair<String, Double>>, Long>(result, count));
			}
			
		});
		return result;
	}
	
	public static void saveToHive(JavaPairRDD<Long, Pair<List<Pair<String, Double>>, Long>> data) {
		System.out.println("Start writing data to seed table " + (new Date()).toString());
		hiveContext.sql("USE lookup_tables");
		hiveContext.sql("CREATE TABLE IF NOT EXISTS seeds (id BIGINT, location STRING, count BIGINT, original INT)");
		List<StructField> fields = new ArrayList<StructField>();
		fields.add(DataTypes.createStructField("id", DataTypes.LongType, false));
		fields.add(DataTypes.createStructField("location", DataTypes.StringType, false));
		fields.add(DataTypes.createStructField("count", DataTypes.LongType, false));
		fields.add(DataTypes.createStructField("original", DataTypes.IntegerType, false));
		StructType seedSchema = DataTypes.createStructType(fields);
		JavaRDD<Row> seeds = data.map(new Function<Tuple2<Long, Pair<List<Pair<String, Double>>, Long>>, Row>() {
			public Row call(Tuple2<Long, Pair<List<Pair<String, Double>>, Long>> row) throws Exception {
				return RowFactory.create(row._1, row._2.getA().get(0).getA(), row._2.getB(), 1);
			}		
		});
		DataFrame df = hiveContext.createDataFrame(seeds, seedSchema);
		df.write().insertInto("seeds");
		System.out.println("Finish writing data to seed table " + (new Date()).toString());
		System.out.println("Start writing data to probability table " + (new Date()).toString());
		hiveContext.sql("CREATE TABLE IF NOT EXISTS probability_table (id BIGINT, location STRING, probability DOUBLE)");
		fields = new ArrayList<StructField>();
		fields.add(DataTypes.createStructField("id", DataTypes.LongType, false));
		fields.add(DataTypes.createStructField("location", DataTypes.StringType, false));
		fields.add(DataTypes.createStructField("probability", DataTypes.DoubleType, false));
		StructType probabilitySchema = DataTypes.createStructType(fields);
		JavaRDD<Row> probability = data.flatMap(new FlatMapFunction<Tuple2<Long, Pair<List<Pair<String, Double>>, Long>>, Row>() {

			public Iterable<Row> call(Tuple2<Long, Pair<List<Pair<String, Double>>, Long>> row) throws Exception {
				List<Row> rows = new ArrayList<Row>();
				for(int i = 0; i < row._2.getA().size(); i++) {
					rows.add(RowFactory.create(row._1, row._2.getA().get(i).getA(), row._2.getA().get(i).getB()));
				}
				return rows;
			}
			
		});
		df = hiveContext.createDataFrame(probability, probabilitySchema);
		df.write().insertInto("probability_table");
		System.out.println("Finish writing data to probability table " + (new Date()).toString());
	}
}