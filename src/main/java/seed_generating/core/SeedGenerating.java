package seed_generating.core;

import java.util.List;

import org.apache.spark.api.java.JavaPairRDD;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import seed_generating.hive.GetTweetsInfoFromHive;
import seed_generating.utils.Pair;

public class SeedGenerating {
	
	private static double alpha = 0.7;
	private static double beta = 0.3;
	private static double gamma = 0.8;
	
	public static class Parameters {
		@Parameter(names = { "-alpha", "-a" }, required = false, description = "")
		private Double alpha = 0.7;
		@Parameter(names = { "-beta", "-b" }, required = false, description = "")
		private Double beta = 0.3;
		@Parameter(names = { "-gamma", "-g" }, required = false, description = "threshold for location probability")
		private Double gamma = 0.8;
	}
	public static void main(String args[]) {
		Parameters params = new Parameters();
		new JCommander(params, args).parse();
		JavaPairRDD<Long, Pair<List<Pair<String, Double>>, Long>> userToLocData = GetTweetsInfoFromHive.getUserToLocData(10, params.gamma);
		JavaPairRDD<Long, Pair<List<Pair<String, Double>>, Long>> result = userToLocData;
		GetTweetsInfoFromHive.saveToHive(result);
	}
}